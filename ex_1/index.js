/*
* Input
*
*
* STEP
1 tạo 2 biến chứa 2 cạnh góc vuông
2 tạo biến chứa kết quả cần tìm
*
*
* Output: 5
*/
var edge1 = 3;
var edge2 = 4;
var result = null;

result = edge1 * edge1 + edge2 * edge2;
console.log("result: ", result);
result = Math.sqrt(result);
console.log("result: ", result);

/*
 * Bài 2
 *
 *
 * 123 => 6
 * 246 => 12
 *
 *
 *
 *
 *
 *
 *
 */
var number = 246;

var donVi = number % 10; //6

var chuc = Math.floor(number / 10) % 10; //4

var tram = Math.floor(number / 100);
var result2 = donVi + chuc + tram;
console.log("result2: ", result2);
